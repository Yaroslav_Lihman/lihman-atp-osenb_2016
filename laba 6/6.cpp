#include <iostream>
#include <string>

using namespace std;

struct ID
{
	string Type;
	string color;
	int wheels;
	int count;
	string weight;
	int price;
};

int main()
{
	ID array[5];
	
	array[0] = { "Sedan", "blue", 4, 4, "1000 kg", 7000 };
	array[1] = { "Mini-van", "white", 8, 4, "4000 kg", 10000 };
	array[2] = { "Universal", "red", 4, 4, "1200 kg", 4500 };
	array[3] = { "Hatchback", "black", 4, 4, "950 kg", 8000 };
	array[4] = { "Cupe", "yellow", 4, 4, "900 kg", 6000 };

	int maxPrice = 0;
	int maxIndex = 0;
	
	for (int i = 0; i < 5; i++)
	{
		if (array[i].price > maxPrice)
		{
			maxPrice = array[i].price;
			maxIndex = i;
		}
	}
	
	cout << "The car with the highest price is " << array[maxIndex].Type << ". Its pirce is " << array[maxIndex].price << " dollars.";

	cin.get();
	return 0;
}
