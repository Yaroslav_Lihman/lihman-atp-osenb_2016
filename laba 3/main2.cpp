#include<iostream>
#include<math.h>
 
using namespace std;
double e=2.71;
double f1(double z) 
{
	return pow(z,2)-pow(e,z);
}
double f2(double z)
{
	return pow(e,2*(pow(z,2)));	
}
double f3(double z)
{
	return pow(cos(z),4)/pow(z,3);	
}
double f(double z)
{
if (z<0)
	return f1(z);
if (z>=0 && z<=8)
	return f2(z);
if (z>8)
	return f3(z);
}
double ex(double z){
    return log(f(z)) - pow(e,2*f(z));
}
 
int main(){
	double z;
    cin>>z;
    cout<<f(z);
}
