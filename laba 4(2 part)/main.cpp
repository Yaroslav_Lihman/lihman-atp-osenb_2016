#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char** argv) {
	double sum=0;
	double i=0.5;
	while (i<=6){
		sum=sum+(4*pow(i, 2)+6*(pow(i, 3))-2);
		i=i+0.3;
	}
	cout<<sum;
	return 0;
}
